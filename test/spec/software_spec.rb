describe command('docker -v') do
  its(:stdout) { should match '17.06.0-ce' }
end

%w[ansible git npm vagrant].each do |sw|
  describe command("#{sw} --version") do
    its(:exit_status) { should be 0 }
  end
end
