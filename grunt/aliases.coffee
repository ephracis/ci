module.exports = (grunt) ->
  
  release: (action) ->
    bump_action = if action then action else 'minor'
    grunt.task.run [
      "bump-only:#{bump_action}",
      'shell:bump_changelog',
      'bump-commit'
    ]

  docker: (action) ->
    grunt.task.run [
      'githash',
      'env:containers',
      "shell:docker:#{action}"
    ]
