FROM fedora:25
ARG version
LABEL Name="Jupiter CI container" \
      Version="$version"

# environment variables
ENV SSH_AUTH_SOCK='' \
    NODE_PATH='/usr/lib/node_modules' \
    DOCKER_HOST='tcp://docker:2375' \
    DOCKER_VERSION='17.06.0-ce' \
    DOCKER_CHANNEL='stable'

# install dnf packages
RUN dnf update -y && \
    dnf install -y ansible \
                   gcc \
                   git \
                   make \
                   npm \
                   redhat-rpm-config \
                   ruby \
                   ruby-devel \
                   vagrant \
                   vagrant-libvirt && \
    dnf clean all && \
    rm -rf /tmp/{,.[!.],..?}*

# install docker client
RUN curl -fL -o docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/x86_64/docker-${DOCKER_VERSION}.tgz" && \
    tar --extract --file docker.tgz --strip-components 1 --directory /usr/bin/ && \
    rm docker.tgz

# install ruby gems
RUN gem install bundler && \
    gem cleanup all && \
    rm -rf /tmp/{,.[!.],..?}*

# install npm modules
RUN npm install -g dockerfile_lint \
                   grunt-cli && \
    npm cache clean && \
    rm -rf /tmp/{,.[!.],..?}*

# install vagrant box and plugin
RUN vagrant plugin install vagrant-aws && \
    vagrant box add aws https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
