# CI container

This container is used in GitLab CI to run all jobs.

It contains:

- Docker client
- Ansible
- Vagrant with AWS plugin
- Git
- Ruby
- Node with NPM and Grunt

## Development

### Requirements

The following needs to be installed on the workstation:

- Docker
- Node
- Ruby

### Get started

Install all packages:

```shell
npm install
```

The system can be built inside a virtualized lab for testing and development.
Grunt is used for running common tasks:

```shell
grunt lint              # lint the code
grunt build             # build the container
grunt test              # run tests against the container
```

### Release

To release a new version use the following commands:

```shell
# assuming that the current release is 0.0.0
grunt release:major     # bumps to 1.0.0
grunt release:minor     # bumps to 0.1.0
grunt release:patch     # bumps to 0.0.1
grunt release:git       # bumps to 0.0.0-ge96c
grunt release:prepatch  # bumps to 0.0.1-rc.0
grunt release:preminor  # bumps to 0.1.0-rc.0
grunt release:premajor  # bumps to 1.0.0-rc.0
grunt release           # just an alias for grunt release:minor
```
