# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.0 - 2017-08-13
### New
- Docker client 17.06.0.
- Git.
- Node.js and npm with grunt-cli.
- Ruby with bundler.
- Vagrant with plugin for AWS.

